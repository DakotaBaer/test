<?php

class __Mustache_84572c817c4130b7e47b199ba8c5cc57 extends Mustache_Template
{
    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $buffer = '';

        $buffer .= $indent . '<p>Hi, ';
        $value = $this->resolveValue($context->find('name'), $context, $indent);
        $buffer .= htmlspecialchars($value, 2, 'UTF-8');
        $buffer .= '.</p>';

        return $buffer;
    }
}
