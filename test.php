<?php
ini_set('display_errors', 1);
session_start();
$_SESSION['lang'] = 'en-CA';
require_once 'app/library/Twig/Autoloader.php';
Twig_Autoloader::register();
require_once 'app/lang/' . $_SESSION['lang'] . '.php';
        $loader = new Twig_Loader_Filesystem('app/views');
        $twig = new Twig_Environment($loader, array('cache' => 'app/cache'));
        echo $twig->render('index.twig', $lang);