<?php

/**
 * <pre>
 * Axiom Development
 * Furtran 1α
 * Main Index
 * Last Updated: 2013-10-04
 * </pre>
 *
 * @author      $Author: DakotaBaer $
 * @copyright   2013 Axiom Development
 * @since       4th October 2013
 * @version     $Rev: 1 $
 */

ini_set('display_errors', 1);
session_start();
$_SESSION['lang'] = 'en-CA';

class Index
{
    public function __construct()
    {
        require_once 'app/library/Twig/Autoloader.php';
        require_once 'app/handlers/IndexHandler.php';
        require_once 'app/library/toro.php';
        Twig_Autoloader::register();
                
        ToroHook::add('404', function() {
            echo 'Page not found.';
        });
        Toro::serve(array(
            '/' => 'IndexHandler'
        ));
    }
}

new Index;
