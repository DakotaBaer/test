<?php

/**
 * <pre>
 * Axiom Development
 * Furtran 1α
 * Index Handler
 * Last Updated: 2013-10-04
 * </pre>
 *
 * @author      $Author: DakotaBaer $
 * @copyright   2013 Axiom Development
 * @since       4th October 2013
 * @version     $Rev: 1 $
 */
 

class IndexHandler extends Index
{
    public function get()
    {
        require_once 'app/lang/' . $_SESSION['lang'] . '.php';
        $loader = new Twig_Loader_Filesystem('app/views');
        $twig = new Twig_Environment($loader, array('cache' => 'app/cache'));
        echo $twig->render('index.twig', $lang);

    }
}
